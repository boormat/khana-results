# Example app with jest tests

## How to use

Download the example (or clone the repo)[https://github.com/zeit/next.js.git]:

```bash
curl https://codeload.github.com/zeit/next.js/tar.gz/master | tar -xz --strip=2 next.js-master/examples/with-jest
cd with-jest
```

Install it and test:

```bash
npm install
npm test
```

## The idea behind the example

This example features:

* An app with jest tests


## HACKAGE
Watch out for 2.0.0 pre-release doco, will give you a bad day!
  "presets": ["es2015", "react"],
      "presets": ["next/babel"]

