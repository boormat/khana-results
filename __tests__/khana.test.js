
import meh from '../data/khana1'

// test('a test', () => {
//     expect(1 + 3).toBe(4);
//     console.log(meh);
//     // expect(meh.results)
// })
import countTests from '../results/countTests'

const td = {
    entrants: [
        { results: [{}, {}, {}] },
        { results: [{},] },
        { results: [{},] }
    ]
}



describe('Results', () => {
    it("countTests", () => {
        let r = countTests(td)
        expect(r.done).toBe(1)
        expect(r.total).toBe(3)
    })

    // it("bestTimes", () => {
    //     let r = bestTimes(td, 1)

    //     expect(r.done).toBe(1)
    //     expect(r.total).toBe(3)
    // })
})