
const results = {
    name: "khana 1",
    date: "meh",
    entrants: [
        {
            name: "Bill",
            number: "01",
            classes: ["outright", "junior", "rally"],
            results: [ // array of test results.
                {   // implied test #1
                    flags: 1,
                    time: 1.0,
                    score: 1,
                    runningScore: 1,
                    // pos counts makes no sense as depends on class filters
                },
                {   // implied test #2
                    flags: 0,
                    time: 9.0,
                    score: 9,
                    runningScore: 10,
                    // pos counts makes no sense as depends on class filters
                }
            ]
        }, {
            name: "Ted",
            number: "2",
            classes: ["outright", "rally"],
            results: [ // array of test results.
                {   // implied test #1
                    flags: 0,
                    time: 2.0,
                    score: 1,           // calculate when building table?
                    runningScore: 1,      // calculate when building table?
                    // pos counts makes no sense as depends on class filters
                },
                {   // implied test #2
                    flags: 0,
                    time: 7.0,
                    score: 9,
                    runningScore: 10,
                    // pos counts makes no sense as depends on class filters
                }
            ]
        }
    ]
};

export default results;