// Count tests in the khana data


/**
 * Count the number of tests in a set of results.
 * @param result Object of results type. 
 * @return  {{done: number, total:number}} number of tests with results

 */

export default (result) => {
    // testCounts = result.entrants.map
    let counts = result.entrants.map(  s => 1 );
    counts = result.entrants.map(  s => s.results.length );
    //min = counts.reduce( (a,b) => Math.min(a,b))
    let done = Math.min(...counts)
    let total = Math.max(...counts)
    return {done, total}
}

